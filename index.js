let students = [];

function addStudent(name) {
	let capName = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
	students.push(capName);
	console.log(`${capName} was added to the student's list.`);

}

function countStudents() {
	console.log(`There are a total of ${students.length} students enrolled`);
}

function printStudents() {

	students.sort();

	students.forEach(function (name) {
		console.log(name);
	});
}

function findStudent(keyword){

	let newkey = keyword.toLowerCase();

	let filteredValid = students.filter(function(name) {

		return name.toLowerCase().startsWith(newkey);
	})

	
	if (filteredValid.length == 1) {
		console.log(`${filteredValid} is an enrolee.`)
	} else if (filteredValid.length > 1) {
		filteredValid.toString();
		console.log(`${filteredValid} are enrolees.`)
	} else if (filteredValid == 0) {
		console.log(`${keyword} is not an enrolee.`)
	}
}

// Stretch Goals

function addSection(section) {

	let x = students.map(function(name) {
			return (`${name} - section ${section}`)
		})

	console.log(x);
}


function removeStudent(name) {
	let capName = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
	let firstIndex = students.indexOf(capName);

	
	if (firstIndex == -1) {
		console.log(`${capName} is not an enrolee.`)
	} else {
		students.splice(firstIndex, 1);
		console.log(`${capName} was removed from student's list.`)
	}
}


